from django.urls import path
from todos.views import todo_list_list, todo_detail, todo_create


urlpatterns = [
    path("create/", todo_create, name="todo_create"),
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_detail, name="todo_detail"),
]
